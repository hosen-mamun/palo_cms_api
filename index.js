const express = require("express");
const http = require("axios");
const bodyParser = require("body-parser");
const querystring = require("querystring");
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const port = 5000;

app.get("/api/stories", async (req, res) => {
  try {
    const fields = req.query.fields;
    const section_id = req.query.section_id;
    const limit = req.query.limit;
    const offset = req.query.offset;
    const url = `https://prothomalo-web.qtstage.io/api/v1/stories?fields=${fields}&section_id=${section_id}&limit=${limit}&offset=${offset}`;
    const { data } = await http.get(url);
    res.header("Access-Control-Allow-Origin", "*");
    res.send(data);
  } catch (error) {
    console.log(error);
  }
});
const server = app.listen(port, () =>
  console.log(`Listening on port ${port}...`)
);

module.exports = server;
